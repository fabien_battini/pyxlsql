# ---------------------------------------------------------------------------------------------------------------
# PyxlSQL project
# This program and library is licenced under the European Union Public Licence v1.2 (see LICENCE)
# developed by fabien.battini@gmail.com
# ---------------------------------------------------------------------------------------------------------------

class PyxlVersion:
    help = '1.10 at 04/30/2023 01:29:26''sp'
    spec = '1.10'
    release = 1
    version = 10
    patch = 0
    date = '04/30/2023 01:29:26'
    def __init__(self):
        pass


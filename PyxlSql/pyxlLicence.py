# ---------------------------------------------------------------------------------------------------------------
# PyxlSQL project
# This program and library is licenced under the European Union Public Licence v1.2 (see LICENCE)
# developed by fabien.battini@gmail.com
# ---------------------------------------------------------------------------------------------------------------

class PyxlLicence:
    def __init__(self):
        pass

    @staticmethod
    def print():
        print("This program and library is licenced under the European Union Public Licence v1.2")			# noqa
        print("")			# noqa
        print("The full text of EUPL V1.2 can be found, in several languages at:")			# noqa
        print("https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12")			# noqa
        print("")			# noqa
        print("and was published on the Official Journal of the European Union 19.5.2017")			# noqa
        print("")			# noqa
        print("                      EUROPEAN UNION PUBLIC LICENCE v. 1.2")			# noqa
        print("                      EUPL © the European Union 2007, 2016")			# noqa
        print("")			# noqa
        print("This European Union Public Licence (the ‘EUPL’) applies to the Work (as defined")			# noqa
        print("below) which is provided under the terms of this Licence. Any use of the Work,")			# noqa
        print("other than as authorised under this Licence is prohibited (to the extent such")			# noqa
        print("use is covered by a right of the copyright holder of the Work).")			# noqa
        print("")			# noqa
        print("The Work is provided under the terms of this Licence when the Licensor (as")			# noqa
        print("defined below) has placed the following notice immediately following the")			# noqa
        print("copyright notice for the Work:")			# noqa
        print("")			# noqa
        print("        Licensed under the EUPL")			# noqa
        print("")			# noqa
        print("or has expressed by any other means his willingness to license under the EUPL.")			# noqa
        print("")			# noqa
        print("1. Definitions")			# noqa
        print("")			# noqa
        print("In this Licence, the following terms have the following meaning:")			# noqa
        print("")			# noqa
        print("- ‘The Licence’: this Licence.")			# noqa
        print("")			# noqa
        print("- ‘The Original Work’: the work or software distributed or communicated by the")			# noqa
        print("  Licensor under this Licence, available as Source Code and also as Executable")			# noqa
        print("  Code as the case may be.")			# noqa
        print("")			# noqa
        print("- ‘Derivative Works’: the works or software that could be created by the")			# noqa
        print("  Licensee, based upon the Original Work or modifications thereof. This Licence")			# noqa
        print("  does not define the extent of modification or dependence on the Original Work")			# noqa
        print("  required in order to classify a work as a Derivative Work; this extent is")			# noqa
        print("  determined by copyright law applicable in the country mentioned in Article 15.")			# noqa
        print("")			# noqa
        print("- ‘The Work’: the Original Work or its Derivative Works.")			# noqa
        print("")			# noqa
        print("- ‘The Source Code’: the human-readable form of the Work which is the most")			# noqa
        print("  convenient for people to study and modify.")			# noqa
        print("")			# noqa
        print("- ‘The Executable Code’: any code which has generally been compiled and which is")			# noqa
        print("  meant to be interpreted by a computer as a program.")			# noqa
        print("")			# noqa
        print("- ‘The Licensor’: the natural or legal person that distributes or communicates")			# noqa
        print("  the Work under the Licence.")			# noqa
        print("")			# noqa
        print("- ‘Contributor(s)’: any natural or legal person who modifies the Work under the")			# noqa
        print("  Licence, or otherwise contributes to the creation of a Derivative Work.")			# noqa
        print("")			# noqa
        print("- ‘The Licensee’ or ‘You’: any natural or legal person who makes any usage of")			# noqa
        print("  the Work under the terms of the Licence.")			# noqa
        print("")			# noqa
        print("- ‘Distribution’ or ‘Communication’: any act of selling, giving, lending,")			# noqa
        print("  renting, distributing, communicating, transmitting, or otherwise making")			# noqa
        print("  available, online or offline, copies of the Work or providing access to its")			# noqa
        print("  essential functionalities at the disposal of any other natural or legal")			# noqa
        print("  person.")			# noqa
        print("")			# noqa
        print("2. Scope of the rights granted by the Licence")			# noqa
        print("")			# noqa
        print("The Licensor hereby grants You a worldwide, royalty-free, non-exclusive,")			# noqa
        print("sublicensable licence to do the following, for the duration of copyright vested")			# noqa
        print("in the Original Work:")			# noqa
        print("")			# noqa
        print("- use the Work in any circumstance and for all usage,")			# noqa
        print("- reproduce the Work,")			# noqa
        print("- modify the Work, and make Derivative Works based upon the Work,")			# noqa
        print("- communicate to the public, including the right to make available or display")			# noqa
        print("  the Work or copies thereof to the public and perform publicly, as the case may")			# noqa
        print("  be, the Work,")			# noqa
        print("- distribute the Work or copies thereof,")			# noqa
        print("- lend and rent the Work or copies thereof,")			# noqa
        print("- sublicense rights in the Work or copies thereof.")			# noqa
        print("")			# noqa
        print("Those rights can be exercised on any media, supports and formats, whether now")			# noqa
        print("known or later invented, as far as the applicable law permits so.")			# noqa
        print("")			# noqa
        print("In the countries where moral rights apply, the Licensor waives his right to")			# noqa
        print("exercise his moral right to the extent allowed by law in order to make effective")			# noqa
        print("the licence of the economic rights here above listed.")			# noqa
        print("")			# noqa
        print("The Licensor grants to the Licensee royalty-free, non-exclusive usage rights to")			# noqa
        print("any patents held by the Licensor, to the extent necessary to make use of the")			# noqa
        print("rights granted on the Work under this Licence.")			# noqa
        print("")			# noqa
        print("3. Communication of the Source Code")			# noqa
        print("")			# noqa
        print("The Licensor may provide the Work either in its Source Code form, or as")			# noqa
        print("Executable Code. If the Work is provided as Executable Code, the Licensor")			# noqa
        print("provides in addition a machine-readable copy of the Source Code of the Work")			# noqa
        print("along with each copy of the Work that the Licensor distributes or indicates, in")			# noqa
        print("a notice following the copyright notice attached to the Work, a repository where")			# noqa
        print("the Source Code is easily and freely accessible for as long as the Licensor")			# noqa
        print("continues to distribute or communicate the Work.")			# noqa
        print("")			# noqa
        print("4. Limitations on copyright")			# noqa
        print("")			# noqa
        print("Nothing in this Licence is intended to deprive the Licensee of the benefits from")			# noqa
        print("any exception or limitation to the exclusive rights of the rights owners in the")			# noqa
        print("Work, of the exhaustion of those rights or of other applicable limitations")			# noqa
        print("thereto.")			# noqa
        print("")			# noqa
        print("5. Obligations of the Licensee")			# noqa
        print("")			# noqa
        print("The grant of the rights mentioned above is subject to some restrictions and")			# noqa
        print("obligations imposed on the Licensee. Those obligations are the following:")			# noqa
        print("")			# noqa
        print("Attribution right: The Licensee shall keep intact all copyright, patent or")			# noqa
        print("trademarks notices and all notices that refer to the Licence and to the")			# noqa
        print("disclaimer of warranties. The Licensee must include a copy of such notices and a")			# noqa
        print("copy of the Licence with every copy of the Work he/she distributes or")			# noqa
        print("communicates. The Licensee must cause any Derivative Work to carry prominent")			# noqa
        print("notices stating that the Work has been modified and the date of modification.")			# noqa
        print("")			# noqa
        print("Copyleft clause: If the Licensee distributes or communicates copies of the")			# noqa
        print("Original Works or Derivative Works, this Distribution or Communication will be")			# noqa
        print("done under the terms of this Licence or of a later version of this Licence")			# noqa
        print("unless the Original Work is expressly distributed only under this version of the")			# noqa
        print("Licence — for example by communicating ‘EUPL v. 1.2 only’. The Licensee")			# noqa
        print("(becoming Licensor) cannot offer or impose any additional terms or conditions on")			# noqa
        print("the Work or Derivative Work that alter or restrict the terms of the Licence.")			# noqa
        print("")			# noqa
        print("Compatibility clause: If the Licensee Distributes or Communicates Derivative")			# noqa
        print("Works or copies thereof based upon both the Work and another work licensed under")			# noqa
        print("a Compatible Licence, this Distribution or Communication can be done under the")			# noqa
        print("terms of this Compatible Licence. For the sake of this clause, ‘Compatible")			# noqa
        print("Licence’ refers to the licences listed in the appendix attached to this Licence.")			# noqa
        print("Should the Licensee's obligations under the Compatible Licence conflict with")			# noqa
        print("his/her obligations under this Licence, the obligations of the Compatible")			# noqa
        print("Licence shall prevail.")			# noqa
        print("")			# noqa
        print("Provision of Source Code: When distributing or communicating copies of the Work,")			# noqa
        print("the Licensee will provide a machine-readable copy of the Source Code or indicate")			# noqa
        print("a repository where this Source will be easily and freely available for as long")			# noqa
        print("as the Licensee continues to distribute or communicate the Work.")			# noqa
        print("")			# noqa
        print("Legal Protection: This Licence does not grant permission to use the trade names,")			# noqa
        print("trademarks, service marks, or names of the Licensor, except as required for")			# noqa
        print("reasonable and customary use in describing the origin of the Work and")			# noqa
        print("reproducing the content of the copyright notice.")			# noqa
        print("")			# noqa
        print("6. Chain of Authorship")			# noqa
        print("")			# noqa
        print("The original Licensor warrants that the copyright in the Original Work granted")			# noqa
        print("hereunder is owned by him/her or licensed to him/her and that he/she has the")			# noqa
        print("power and authority to grant the Licence.")			# noqa
        print("")			# noqa
        print("Each Contributor warrants that the copyright in the modifications he/she brings")			# noqa
        print("to the Work are owned by him/her or licensed to him/her and that he/she has the")			# noqa
        print("power and authority to grant the Licence.")			# noqa
        print("")			# noqa
        print("Each time You accept the Licence, the original Licensor and subsequent")			# noqa
        print("Contributors grant You a licence to their contributions to the Work, under the")			# noqa
        print("terms of this Licence.")			# noqa
        print("")			# noqa
        print("7. Disclaimer of Warranty")			# noqa
        print("")			# noqa
        print("The Work is a work in progress, which is continuously improved by numerous")			# noqa
        print("Contributors. It is not a finished work and may therefore contain defects or")			# noqa
        print("‘bugs’ inherent to this type of development.")			# noqa
        print("")			# noqa
        print("For the above reason, the Work is provided under the Licence on an ‘as is’ basis")			# noqa
        print("and without warranties of any kind concerning the Work, including without")			# noqa
        print("limitation merchantability, fitness for a particular purpose, absence of defects")			# noqa
        print("or errors, accuracy, non-infringement of intellectual property rights other than")			# noqa
        print("copyright as stated in Article 6 of this Licence.")			# noqa
        print("")			# noqa
        print("This disclaimer of warranty is an essential part of the Licence and a condition")			# noqa
        print("for the grant of any rights to the Work.")			# noqa
        print("")			# noqa
        print("8. Disclaimer of Liability")			# noqa
        print("")			# noqa
        print("Except in the cases of wilful misconduct or damages directly caused to natural")			# noqa
        print("persons, the Licensor will in no event be liable for any direct or indirect,")			# noqa
        print("material or moral, damages of any kind, arising out of the Licence or of the use")			# noqa
        print("of the Work, including without limitation, damages for loss of goodwill, work")			# noqa
        print("stoppage, computer failure or malfunction, loss of data or any commercial")			# noqa
        print("damage, even if the Licensor has been advised of the possibility of such damage.")			# noqa
        print("However, the Licensor will be liable under statutory product liability laws as")			# noqa
        print("far such laws apply to the Work.")			# noqa
        print("")			# noqa
        print("9. Additional agreements")			# noqa
        print("")			# noqa
        print("While distributing the Work, You may choose to conclude an additional agreement,")			# noqa
        print("defining obligations or services consistent with this Licence. However, if")			# noqa
        print("accepting obligations, You may act only on your own behalf and on your sole")			# noqa
        print("responsibility, not on behalf of the original Licensor or any other Contributor,")			# noqa
        print("and only if You agree to indemnify, defend, and hold each Contributor harmless")			# noqa
        print("for any liability incurred by, or claims asserted against such Contributor by")			# noqa
        print("the fact You have accepted any warranty or additional liability.")			# noqa
        print("")			# noqa
        print("10. Acceptance of the Licence")			# noqa
        print("")			# noqa
        print("The provisions of this Licence can be accepted by clicking on an icon ‘I agree’")			# noqa
        print("placed under the bottom of a window displaying the text of this Licence or by")			# noqa
        print("affirming consent in any other similar way, in accordance with the rules of")			# noqa
        print("applicable law. Clicking on that icon indicates your clear and irrevocable")			# noqa
        print("acceptance of this Licence and all of its terms and conditions.")			# noqa
        print("")			# noqa
        print("Similarly, you irrevocably accept this Licence and all of its terms and")			# noqa
        print("conditions by exercising any rights granted to You by Article 2 of this Licence,")			# noqa
        print("such as the use of the Work, the creation by You of a Derivative Work or the")			# noqa
        print("Distribution or Communication by You of the Work or copies thereof.")			# noqa
        print("")			# noqa
        print("11. Information to the public")			# noqa
        print("")			# noqa
        print("In case of any Distribution or Communication of the Work by means of electronic")			# noqa
        print("communication by You (for example, by offering to download the Work from a")			# noqa
        print("remote location) the distribution channel or media (for example, a website) must")			# noqa
        print("at least provide to the public the information requested by the applicable law")			# noqa
        print("regarding the Licensor, the Licence and the way it may be accessible, concluded,")			# noqa
        print("stored and reproduced by the Licensee.")			# noqa
        print("")			# noqa
        print("12. Termination of the Licence")			# noqa
        print("")			# noqa
        print("The Licence and the rights granted hereunder will terminate automatically upon")			# noqa
        print("any breach by the Licensee of the terms of the Licence.")			# noqa
        print("")			# noqa
        print("Such a termination will not terminate the licences of any person who has")			# noqa
        print("received the Work from the Licensee under the Licence, provided such persons")			# noqa
        print("remain in full compliance with the Licence.")			# noqa
        print("")			# noqa
        print("13. Miscellaneous")			# noqa
        print("")			# noqa
        print("Without prejudice of Article 9 above, the Licence represents the complete")			# noqa
        print("agreement between the Parties as to the Work.")			# noqa
        print("")			# noqa
        print("If any provision of the Licence is invalid or unenforceable under applicable")			# noqa
        print("law, this will not affect the validity or enforceability of the Licence as a")			# noqa
        print("whole. Such provision will be construed or reformed so as necessary to make it")			# noqa
        print("valid and enforceable.")			# noqa
        print("")			# noqa
        print("The European Commission may publish other linguistic versions or new versions of")			# noqa
        print("this Licence or updated versions of the Appendix, so far this is required and")			# noqa
        print("reasonable, without reducing the scope of the rights granted by the Licence. New")			# noqa
        print("versions of the Licence will be published with a unique version number.")			# noqa
        print("")			# noqa
        print("All linguistic versions of this Licence, approved by the European Commission,")			# noqa
        print("have identical value. Parties can take advantage of the linguistic version of")			# noqa
        print("their choice.")			# noqa
        print("")			# noqa
        print("14. Jurisdiction")			# noqa
        print("")			# noqa
        print("Without prejudice to specific agreement between parties,")			# noqa
        print("")			# noqa
        print("- any litigation resulting from the interpretation of this License, arising")			# noqa
        print("  between the European Union institutions, bodies, offices or agencies, as a")			# noqa
        print("  Licensor, and any Licensee, will be subject to the jurisdiction of the Court")			# noqa
        print("  of Justice of the European Union, as laid down in article 272 of the Treaty on")			# noqa
        print("  the Functioning of the European Union,")			# noqa
        print("")			# noqa
        print("- any litigation arising between other parties and resulting from the")			# noqa
        print("  interpretation of this License, will be subject to the exclusive jurisdiction")			# noqa
        print("  of the competent court where the Licensor resides or conducts its primary")			# noqa
        print("  business.")			# noqa
        print("")			# noqa
        print("15. Applicable Law")			# noqa
        print("")			# noqa
        print("Without prejudice to specific agreement between parties,")			# noqa
        print("")			# noqa
        print("- this Licence shall be governed by the law of the European Union Member State")			# noqa
        print("  where the Licensor has his seat, resides or has his registered office,")			# noqa
        print("")			# noqa
        print("- this licence shall be governed by Belgian law if the Licensor has no seat,")			# noqa
        print("  residence or registered office inside a European Union Member State.")			# noqa
        print("")			# noqa
        print("Appendix")			# noqa
        print("")			# noqa
        print("‘Compatible Licences’ according to Article 5 EUPL are:")			# noqa
        print("")			# noqa
        print("- GNU General Public License (GPL) v. 2, v. 3")			# noqa
        print("- GNU Affero General Public License (AGPL) v. 3")			# noqa
        print("- Open Software License (OSL) v. 2.1, v. 3.0")			# noqa
        print("- Eclipse Public License (EPL) v. 1.0")			# noqa
        print("- CeCILL v. 2.0, v. 2.1")			# noqa
        print("- Mozilla Public Licence (MPL) v. 2")			# noqa
        print("- GNU Lesser General Public Licence (LGPL) v. 2.1, v. 3")			# noqa
        print("- Creative Commons Attribution-ShareAlike v. 3.0 Unported (CC BY-SA 3.0) for")			# noqa
        print("  works other than software")			# noqa
        print("- European Union Public Licence (EUPL) v. 1.1, v. 1.2")			# noqa
        print("- Québec Free and Open-Source Licence — Reciprocity (LiLiQ-R) or Strong")			# noqa
        print("  Reciprocity (LiLiQ-R+).")			# noqa
        print("")			# noqa
        print("The European Commission may update this Appendix to later versions of the above")			# noqa
        print("licences without producing a new version of the EUPL, as long as they provide")			# noqa
        print("the rights granted in Article 2 of this Licence and protect the covered Source")			# noqa
        print("Code from exclusive appropriation.")			# noqa
        print("")			# noqa
        print("All other changes or additions to this Appendix require the production of a new")			# noqa
        print("EUPL version.")			# noqa
    # End of patched text

# ---------------------------------------------------------------------------------------------------------------
# PyxlSQL project
# This program and library is licenced under the European Union Public Licence v1.2 (see LICENCE)
# developed by fabien.battini@gmail.com
# ---------------------------------------------------------------------------------------------------------------

from PyxlSql.pyxlSql import run_pyxl_sql

if __name__ == "__main__":
    run_pyxl_sql()
Developing
##########

If you want to develop:

* Clone the repository from gitlab
* Whenever possible, use PyCharm as an IDE
* in the project configuration
    * Choose a python interpreter
    * Install tox (4.4.7) :     cmd> python -m pip install tox
    * run tox, this should install all dependencies  cmd> tox

If you want to test the files locally in your development environment
    * use tox, it does everything, safely
    * (cmd) PyxlSQL> tox

If you want to use the pyxlSql version under development from a windows CMD in the project root directory
    * (cmd) PyxlSQL> python tests\test_Northwind.py
    * (cmd) PyxlSQL> python -m PyxlSql tests/Test_pyxlsql.xlsx
    * (cmd) PyxlSQL> pytest --cache-clear --ff
    * (cmd) PyxlSQL> pytest --flakes
    * (cmd) PyxlSQL> pytest --cov --cov-report html --cov-report term
    * (cmd) PyxlSQL> sphinx-build source build/html -W -b html

Normal development flow:
    * update the requirement with a TODO statement, either in pyxlSql.py or in a logical file
    * develop
    * commit locally if useful, without tag
    * maybe commit to gitlab. if there is not tag, the CI pipeline is not run, so public doc is not updated
    * test with tox
    * maybe put a patch tag with 'python patch_files.py -p', this will
        * NOT change download.rst
        * commit locally
    * if this commit and tag is pushed to gitlab, the doc is modified, which may be a problem
    * when all tests are OK and development can be delivered to customers:
        * put a tag with 'python patch_files.py -m' or 'python patch_files.py -M'
        * push to gitlab, that updates the doc
        * run 'python upload.py --Pypi --GitLab', that uploads the new module and the new exec
    * from time to time, clean download.rst, because its stacks all versions


Documentation is built using Sphinx https://www.sphinx-doc.org/

gitlab repository: https://gitlab.com/fabien_battini/pyxlsql

Documentation is generated in https://fabien_battini.gitlab.io/pyxlsql/html/

    This process is achieved automatically through gitlab CI pipelines,
    each time a commit is pushed to the gitlab site (see  .gitlab-ci.yml and
    https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_ui.html )

pypi: https://pypi.org/project/excel-sql-engine/

    See make.bat for the process

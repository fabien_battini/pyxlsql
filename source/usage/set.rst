SET
###

The SET clause is a subcomponent of SELECT or UPDATE statements

SET is stolen from the SQL syntax for UPDATE and extended to SELECT

Syntax::

   Set_clause     := "SET" dst_field "=" src_value ["WHEN" src_expr(1,2)])  |
                                       "AGGREGATES" expr() "WITH" red_expr   |
                                       Aggregation_key expr()                )

dst_field is the field to be updated
src_value can be:

 - a field name
 - a Python expression, starting with ':= '
 - an Excel formula, starting with '='

WHEN specifies a condition, if false, the SET clause is not executed

src_expr is an expression, executed in the context of the sources of the enclosing statement


AGGREGATES or Aggregation_key are used what SET is executed after a GROUP BY clause

red_expr is a reduction expression starting with '$= ', it is applied to the *list* of fields/expressions

Aggregation_key can be MIN, MAX, COUNT, AVG, SUM

for instance, the following expressions are equivalent:

===========     ========   ============    ===========  =========   =====================================
STATEMENT    	First	   KEY	            Second      CONDITION   Third
===========     ========   ============    ===========  =========   =====================================
SET             foo        COUNT           item
SET             foo        AGGREGATES      item          WITH       $= len(#$)

SET             foo        AVERAGE         item
SET             foo        AGGREGATES      item          WITH       $= sum(#$)/len(#$) if len(#$) else 0

SET             foo        MIN             item
SET             foo        AGGREGATES      item          WITH       $= functools.reduce(min, #$)

SET             foo        MAX             item
SET             foo        AGGREGATES      item          WITH       $= functools.reduce(max, #$)

SET             foo        SUM             item
SET             foo        AGGREGATES      item          WITH       $= sum(#$)
===========     ========   ============    ===========  =========   =====================================


AGGREGATES can aggregate an expression, which is more powerful than aggregating an existing field

Example from test_pyxlsql.xlsx[Pyxl SQL]:

===========     =================================   ==========    ================================================  ==========   ===========
STATEMENT    	First	                            KEY	          Second                                            CONDITION    Third
===========     =================================   ==========    ================================================  ==========   ===========
UPDATE	        USA commands
LEFT JOIN       Northwind-V2.xlsx[Orders Details]
ON              @0{Order UID}	                    =	          @1{Order ID}
SET         	Quantities                          SUM	          @1{Quantity}
SET             Total	                            AGGREGATES	  @1{Unit Price} * @1{Quantity}* (1- @1{Discount})  WITH	     $= sum(#$)
FORMAT	        Total	                            =	          $7,688
GROUP BY        @0{Order UID}
===========     =================================   ==========    ================================================  ==========   ===========

See also: :doc:`group_by`

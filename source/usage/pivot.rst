PIVOT
#####

PIVOT creates a mini pivot table in a dedicated sheet, from data in a source sheet.


This includes:

* column C: All possible values for the Field declared in C1

* column E: The sum for all values of the column declared in E1

* column D: the corresponding percentage

* column A: possible values for the field declared in A1

* column B: dynamic filters '' means: exclude, non-empty means include

* column J: a pie-chart, with assigned colors

Caveat::

    This mini pivot is NOT a regular Excel pivot table,
    it is rather a minimal implementation, using only excel traditional functions
    and formulas

The syntax of the sheet is different from other Pyxl SQL sheets, and described with the following example:

The Pyxl SQL sheet holds the following code, which means create a mini pivot in sheet 'Pivot',
from data in sheet 'Market':


===========     =========================   ====    ====================
STATEMENT       First                       KEY     Second
===========     =========================   ====    ====================
COMMENTS        Testing PIVOT command
PIVOT           Pivot                       FROM    Market
===========     =========================   ====    ====================

The Market sheet holds the following columns:

=======  ====  ==========   =====  =====  ========
Country  Area  Basic 2019   Ratio  Range  Priority
=======  ====  ==========   =====  =====  ========


The sheet `Test_pyxlsql.xlsx[Pivot] <../examples/Test_pyxlsql.html#Pivot>`_  initially holds the  following code:

======  ==============  ============ ========  ==========  ==========  ========  ============ =========
A       B               C            D         E           F           G         H             I
======  ==============  ============ ========  ==========  ==========  ========  ============ =========
Range   Range Y/(N='')  Country      Percents  Basic 2019  Exclude if  Field is  Field is not  Comments
None    None            USA          12,3%     125 007     Priority    Low
-                       Canada                             Country     Borneo
-                       Argentina
-                       Belize
-                       Brazil
-                       etc
-                       Switzerland
======  ==============  ============ ========  ==========  ==========  ========  ============ =========

Line 1 holds items that you can freely edit, and that are meaningful for the construction of the mini-pivot table:

* A1: Range. Declares that Range is a field of the Market Sheet that will be used to dynamically filter data

* B1: Range Y/(N='').  A field that you can edit as an explanation of the column


* C1: Country. Declares that the mini-pivot table will sum all 'Basic 2019' values, for each Country

    C2, C3 etc: In the initial column, some values for 'Country' are already written, and are formatted with a background color.
    This color will be used as the Country color in the generated pie-chart
    The column will also be completed with all possible values of 'Country'

* D1: Percentage:  A field that you can edit as an explanation of the column

* E1: Basic 2019: The field that will be used for sums

* F1: A field that you can edit as an explanation of the column

    F2 (Priority) and following values in the F column are Fields names in the 'Market' sheet

    Column G and H holds values that are used to statically exclude or lines where this field has, or has not this value

* G1: A field that you can edit as an explanation of the column

    G2: Excludes lines with Low priority

    G3 Excludes lines with Country Borneo


* H1: A field that you can edit as an explanation of the column


The final sheet, `northwind-results.xlsx[Pivot] <../examples/northwind-results.html#Pivot>`_ after execution of the PIVOT command, looks like:

======  ==============  ============ ========  ==========  ==========  ========  ============ =========
A       B               C            D         E           F           G         H             I
======  ==============  ============ ========  ==========  ==========  ========  ============ =========
Range   Range Y/(N='')  Country      Percents  Basic 2019  Exclude if  Field is  Field is not Comments
Medium  Y               USA          18,0%        555 798  Priority    Low
High    Y               Canada        5,0%        154 987  Country     Borneo
Low     Y               Argentina     0,3%         10 250
-                       Belize        0,2%          5 324
-                       Brazil       11,1%        345 287
-                       etc
-                       Switzerland   0,8%         25 258
======  ==============  ============ ========  ==========  ==========  ========  ============ =========



And a pie chart representing Columns (Country, Percent) is inserted on cell J2

* Column A:  will be populated with all possible values of Range

* Column B: for each row with a Range value, the column will be populated with 'Y'. Then the user CAN change Y to '' (empty)

    * Any value, e.g. 'Y' means that lines in the 'market' sheet, with the corresponding Range, is included in the computation

    * '' means that any line with the corresponding Range is excluded

* Column C: will hold all values for 'Country' that are actually used, after filters

* Column D: percentage

* Column E: The sum of "Basic 2019" for all lines that has the corresponding Country
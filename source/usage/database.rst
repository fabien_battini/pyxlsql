DATABASE
########

DATABASE is a pyxlSQL specific statement, without SQL equivalent, and without clauses

DATABASE load data from an Excel sheet by specifying non default start and end

start is the 1st useful cell, containing column names
end is the last useful cell, containing column names or data

the format of start and end is the traditional letter + number format, e.g. A1, BE123 etc.

if data is stored in the Excel sheet using the default place, i.e. starting at A1 and ending with empty row and column,
then there is no need to use this statement


Syntax::

    Dababase_cmd   := "DATABASE" src_sheet "START" cell "END" cell

Example from test_pyxlsql.xlsx[Pyxl SQL]:

=========     ==============   ======  =========   =========    ====================
STATEMENT     First            KEY     Second      CONDITION    Third
=========     ==============   ======  =========   =========    ====================
DATABASE      Unaligned        START   B6          END          N1006
DATABASE      Country Salary   START   A4          END          C4
=========     ==============   ======  =========   =========    ====================

See also   `Test_pyxlsql.xlsx[Unaligned] <../examples/Test_pyxlsql.html#Unaligned>`_
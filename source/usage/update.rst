UPDATE
######

The UPDATE statement is used to modify the existing records in a table.

UPDATE is a statement: it is NOT included in a wider enclosing statement.

Example from Test_pyxlsql.xlsx[Pyxl SQL]:

===========     ==========   ====  =====================================================   =========   =======================================
STATEMENT    	First	     KEY   Second                                                  CONDITION   Third
===========     ==========   ====  =====================================================   =========   =======================================
IMPORT	        datetime     SUBS	*
IMPORT	        os           SUBS	curdir getcwd
IMPORT	        time
COMMENTS                            Previous imports are necessary to expressions below

UPDATE	        Header
SET             Value        =	   := datetime.now().strftime("%d. %B %Y %H:%M:%S")	       WHEN	        := @0{Item} == "Date"
SET             Value        =	   := getcwd()	                                           WHEN	        := @0{Item} == "Current directory"
SET             Value        =	   := time.localtime().tm_year	                           WHEN	        := @0{Item} == "Year"
SET             Value        =	   := "test pyxlSQL"	                                   WHEN	        := @0{Item} == "Purpose"
===========     ==========   ====  =====================================================   =========   =======================================

The initial value of 'header' is:

====    =================   =================
Line    A                   B
====    =================   =================
01      Item	            Value
02      File	            test_pyxlsql.xlsx
03      Date
04      Year
05      Current directory
06      Purpose
====    =================   =================

The final value is:

====    =================   =================
Line    A                   B
====    =================   =================
01      Item	            Value
02      File	            test_pyxlsql.xlsx
03      Date                14. December 2020 23:23:18
04      Year                2020
05      Current directory   foo/PyxlSQL
06      Purpose	            test pyxlSQL
====    =================   =================

Automatic format & formulas
###########################

The the 2nd line of a sheet (i.e. the first line after the headers) contains either excel formulas
or excel formats, an FORMAT clause of a FORMULA clause is automatically added to all SELECT INTO
statements that add lines to this sheet.

Of course, this first "example" line is removed.

in Test_pyxlsql.xlsx[GroupBy]:

====== =============   ===========     =========   ========  =======  =============
Line        A                B          C           D           E       F
====== =============   ===========     =========   ========  =======  =============
01     Info ID	        Country	       2019        2020       2021    Mean
02     1                Argentina       10,250 	   12,300    14,760   =(C2+D2+E2)/3
====== =============   ===========     =========   ========  =======  =============

And cell C2 has a BLUE foreground

When the following SQL statements are executed, the 'Auto' sheet is updated, and the formats and formulas are copied,
so the Formula for Mean value is copied each line,
and the font color of the 2019 column is always Blue


===========     ==========================   ====   ====================
STATEMENT    	First	                     KEY	    Second
===========     ==========================   ====   ====================
SELECT INTO	    Auto
FROM	        Environemental.xlsx[Sales]   AS	    sales
SET             2019	                     =	    #sales{2019}
SET             2020	                     =	    #sales{2020}
SET             2021	                     =	    #1{2021}
UID             Info ID	                     =	    Info @
SET             Country	                     =	    @sales{Country}
WHERE	        := @1{KPI} == "Basic"
===========     ==========================   ====   ====================

see: `northwind-results[Auto] <../examples/northwind-results.html#Auto>`_
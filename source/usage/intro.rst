
Introduction to excel-sql-engine
################################

excel-sql-engine is a command-line tool that implements a Pyxl-SQL engine for Excel workbooks.

No Python is required, you just write SQL code inside Excel and run the tool.

* based on Openpyxl for Excel file format read/write
* applies to excel workbooks that follow a 'table' format with unique column headers
* SQL queries are written inside the 'Pyxl SQL' sheet
* uses python expressions
* uses Excel formulas, with column names
* is typically used to routinely merge/join information from several Excel files into a new one.
* supports traditional SQL: SELECT INTO, UPDATE, FROM, AS, SET, LEFT/RIGHT/FULL/INNER JOIN, ON, WHERE, GROUP BY, ORDER BY, HAVING
* supports extensions specific to Python: Python expressions, IMPORT
* supports extensions for Excel: FORMAT, Excel formulas, automatic FORMAT and formulas, SAVE, DELETE SHEET
* based on Sphinx for the documentation
* repository is gitLab with CI/CD tools


excel-sql-engine can be used as an command-line tool

    In this case, it looks for excel files in the directory and executes commands found on te 'Pyxl SQL'
    sheet. command-line options allow to change the behavior.

excel-sql-engine can also be used as a python library

excel-sql-engine is tested with tox and cov. See the "tests" directory in the source tree

The current coverage is 94%, most of the non-covered code is defensive, see `htmlcov/index.html <htmlcov/index.html>`_

pyxl-sql is documented using Sphinx. https://www.sphinx-doc.org/

doc is generated in https://fabien_battini.gitlab.io/pyxlsql/html/

example of PyxlSQL statements in the [Pyxl SQL] sheet of the tests/Test_pyxlsql.xlsx file

===========     ==================================   ==========  =========================
STATEMENT       First                                KEY         Second
===========     ==================================   ==========  =========================
COMMENTS        HAVING example                       in          the documentation

SELECT INTO     Having                               AS          exo
FROM            Northwind.xlsx[Customers]            AS          customers
SET             Count                                COUNT       @customers{Customer ID}
SET             Country                              =           @customers{Country}
GROUP BY        @customers{Country}
HAVING          := #exo{Count} > 5
ORDER BY        #exo{Count}
===========     ==================================   ==========  =========================

corresponding SQL from https://www.w3schools.com/sql/sql_having.asp::

   SELECT COUNT(CustomerID), Country
   FROM Customers
   GROUP BY Country
   HAVING COUNT(CustomerID) > 5
   ORDER BY COUNT(CustomerID) DESC;


Example of AGGREGATE with several fields, using a Python expression

===========     ==================================   ==========  =========================
STATEMENT       First                                KEY         Second
===========     ==================================   ==========  =========================
UPDATE          USA commands
LEFT JOIN       Northwind-V2.xlsx[Orders Details]
ON              @0{Order UID}                        =           @1{Order ID}
SET             Quantities                           SUM         @1{Quantity}
SET             Total                                AGGREGATES  @1{Unit Price} * @1{Quantity}* (1- @1{Discount})    WITH    $= sum(#$)
GROUP BY        @0{Order UID}
===========     ==================================   ==========  =========================
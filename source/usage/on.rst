ON
##

The ON clause is a sub-component of a JOIN clause, and indicates the condition for the join


Syntax::

    on_clause      := "ON" first_expr(1) "=" second_expr(2)

See also: :doc:`select` for examples
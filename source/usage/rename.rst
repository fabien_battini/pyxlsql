RENAME
######

RENAME is a pyxlSQL specific statement, without SQL equivalent, and without clauses

RENAME changes the name of a SHEET * in the current workbook *

This is typically used to build the final sheet

Syntax::

    Rename_cmd       := "RENAME" old_sheet "AS" new_sheet

RENAME can be used to perform a multi-pass processing:

===========     =========================   ====    ====================
STATEMENT       First                       KEY     Second
===========     =========================   ====    ====================
RENAME          One sheet                   AS      new name
===========     =========================   ====    ====================


Conventions
###########

Sheet format
************

Sheets that contain a database (or SQL Statements) to be used by Pyxl-SQL should be formatted like a database, i.e:
  - 1st line (default) is a column header
  - column headers are all different

however, the DATABASE (:ref:`database`) statement can change this default behavior,
by changing the start and end of the database information within the sheet

Sheets that not referenced by SQL statements are not parsed, and therefore can follow any convention

Field Names
***********

A 'Field' is a column name

* within the same Sheet, all Fields MUST be different
* upper and lower case are meaningful
* spaces and other special characters are allowed,
* however, python metacharacters such as quotes, $, operators etc should be avoided

Each statement will write only 1 Destination-field after reading 0,1 or several Source-Fields
Fields can be specified using several syntax:

*  'field name'
*  '(@|#)N{field name}', with N a sheet number, 0=destination, 1,2 etc = sources
*  '(@|#)alias{field name}', with alias an alisa declared in the 'AS' statement
*  '#' means: a number. in this case, the value will be (float)value
*  '@' or nothing means : a string

It is preferable to 'type' the variables, because the conventions for 'empty cell'

* for Excel: empty is 0 or ""
* when for python, empty is None, and None cannot be part of expressions with strings or numbers
* If there is an ambiguity in the filed name, then an error is raised

Depending on the statement, Fields from the source or the destination can be forbidden at some location

For instance, the 1st argument of a Set Clause MUST be in the Destination sheet

===========     =========================   ====    ====================
STATEMENT    	First	                    KEY	    Second
===========     =========================   ====    ====================
SELECT INTO     Best	                    AS	    best
FROM	        Auto	                    AS	    auto
SET             Local ID                    =       Info ID
===========     =========================   ====    ====================

* 'Local ID' must be in 'best', the Destination
* 'Info ID' must be in 'auto', the source





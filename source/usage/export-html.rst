EXPORT HTML
###########

EXPORT HTML is a pyxlSQL specific statement, without SQL equivalent, and without clauses

EXPORT HTML exports to an html file (arg 1) from a workbook (arg 2)

This is typically used to build the documentation of a pyxlSQL program,
and avoids to use excel to save the workbook as a HTML file

Syntax::

    Export_cmd       := "EXPORT HTML" filename "FROM" src_workbook

Example from test_pyxlsql.xlsx[Pyxl SQL]:

===========     =========================================   ====    =====================
STATEMENT    	First	                                    KEY	    Second
===========     =========================================   ====    =====================
EXPORT HTML     build/html/examples/Test-Html-Export.html   FROM    Test-Html-Export.xlsx
===========     =========================================   ====    =====================

Caveat::

    EXPORT HTML is *not* as complete as the 'export to html' feature of excel
    if one needs a high quality HTML rendering, then the excel solution should be favored

Currently, EXPORT HTML supports rendering of:

* all sheets of the workbook, in a single HTML file
* fonts, when the excel font is compatible with HTML
* font-size
* bold, italics, underline
* left, center, right
* top, middle, bottom
* most integer, float, date formats
* showing Excell expression rather than cell value

All other potential formatting items are not rendered, notably:
* row heights
* column width
* cell merging
* pictures and graphics

See examples of generated HTML in  :ref:`HTML examples`


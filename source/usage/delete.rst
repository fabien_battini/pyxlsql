DELETE SHEET
############

DELETE SHEET is a pyxlSQL specific statement, without SQL equivalent, and without clauses

DELETE SHEET deletes the sheet argument in the excel workbook

This is typically used to delete the 'Pyxl SQL' sheet to build the final sheet

Syntax::

    Delete_cmd     := "DELETE" dst_sheet

Example from test_pyxlsql.xlsx[Pyxl SQL]:

===========     =========================   ====    ====================
STATEMENT    	First	                    KEY	    Second
===========     =========================   ====    ====================
DELETE          Pyxl SQL
===========     =========================   ====    ====================

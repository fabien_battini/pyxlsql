WHERE
#####

The WHERE clause is used to filter records.

The WHERE clause is used to extract only those records that fulfill a specified condition

Syntax::

   Where_clause   := "WHERE" expr(0,1,2)

expr is a Python expression, executed in the context of all sheets, so, in general, the sheets must be named or numbered


Example is also from standard SQL on
https://www.w3schools.com/sql/sql_where.asp

Original SQL::

    SELECT * FROM Customers
    WHERE Country='Mexico';

This translates into the following pyxl-sql in Test_pyxlsql.xlsx[Pyxl SQL]:

===========     ====================================   ====    ==========================
STATEMENT    	First	                                KEY	    Second
===========     ====================================   ====    ==========================
SELECT INTO     Order
FROM            Northwind.xlsx[Customers]               AS      customers
SET         	Customer Name	                        =	    @customers{Customer Name}
SET         	Customer ID                             =	    @customers{Customer ID}
SET         	Contact Name                            =	    @customers{Contact Name}
WHERE           := @customers{Country} == "Mexico"
===========     ====================================   ====    ==========================

.. warning::
    The automatic discovery of fields is *NOT YET IMPLEMENTED*,
    so the explicit list of copied field must be written
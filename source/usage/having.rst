HAVING
######

HAVING plays the same role than WHERE, but with results from GROUPS BY

Syntax::

  Having_clause  := "HAVING" expr(0)


Example from standard SQL on  https://www.w3schools.com/sql/sql_having.asp

Original SQL::

    SELECT COUNT(CustomerID), Country
    FROM Customers
    GROUP BY Country
    HAVING COUNT(CustomerID) > 5
    ORDER BY COUNT(CustomerID) DESC;

This translates into the following pyxl-sql in Test_pyxlsql.xlsx[Pyxl SQL]:

===========     =========================   ======    ====================
STATEMENT    	First	                    KEY	      Second
===========     =========================   ======    ====================
SELECT INTO 	Having	                    AS	      exo
FROM	        Northwind.xlsx[Customers]	AS	      customers
SET             Count                       COUNT	  @customers{Customer ID}
SET             Country	                    =	      @customers{Country}
GROUP BY	    @customers{Country}
HAVING	        := #exo{Count} > 5
ORDER BY	    #exo{Count}
===========     =========================   ======    ====================
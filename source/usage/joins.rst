

LEFT | RIGHT | INNER | FULL JOIN
################################

The JOIN statements are used to combine rows from two or more tables, based on a related column between them
Currently, only LEFT, RIGHT, INNER and FULL JOIN are supported.
    

LEFT JOIN
*********

A LEFT JOIN example can be read on :doc:`select`

RIGHT JOIN
**********

INNER JOIN Example, taken from Test_pyxlsql.xslx

and also demonstrated with standard SQL on
https://www.w3schools.com/sql/sql_join_right.asp
    
The original SQL statement is::

    SELECT Orders.OrderID, Employees.LastName, Employees.FirstName
    FROM Orders
    RIGHT JOIN Employees 
    ON Orders.EmployeeID = Employees.EmployeeID
    ORDER BY Orders.OrderID; 

This translates into the following pyxl-sql in Test_pyxlsql.xlsx[Pyxl SQL]:

==============  ==========================  ====    =========================
STATEMENT       First                       KEY     Second
==============  ==========================  ====    =========================
SELECT INTO     Right Join
FROM            Northwind.xlsx[Orders]      AS      orders
RIGHT JOIN      Northwind.xlsx[Employees]   AS      employees
ON              @orders{Employee ID}        =       @employees{Employee ID}
SET             @exe{Order ID}              =       @orders{Order ID}
SET             Last Name                   =       @employees{Last Name}
SET             First Name                  =       @employees{First Name}
ORDER BY        #exe{Order ID}
==============  ==========================  ====    =========================

The table generated in "Right Join Example" sheet, identical to w3schools:

=========   =========   =========   ===========
Line Nb     Order ID    Last Name   First Name
=========   =========   =========   ===========
002                     West        Adam
003         10248       Buchanan    Steven
004         10249       Suyama      Michael
005         10250       Peacock     Margaret
...
197         10442       Leverling   Janet
198         10443       Callahan    Laura
=========   =========   =========   ===========

 
INNER JOIN
**********

INNER JOIN Example, taken from Test_pyxlsql.xslx

and also demonstrated with standard SQL on
https://www.w3schools.com/sql/sql_join_inner.asp
    
The original SQL statement is::

    SELECT Orders.OrderID, Customers.CustomerName
    FROM Orders
    INNER JOIN Customers ON Orders.CustomerID = Customers.CustomerID;

This translates into the following pyxl-sql in Test_pyxlsql.xlsx[Pyxl SQL]:

==============  ==========================  ====    =========================
STATEMENT       First                       KEY     Second
==============  ==========================  ====    =========================
SELECT INTO     Inner Join                
FROM            Northwind.xlsx[Orders]      AS      orders
RIGHT JOIN      Northwind.xlsx[Customers]   AS      customers
ON              @orders{Customer ID}        =       @customers{Customer ID}
SET             Order ID                    =       #orders{Order ID}
SET             Customer Name               =       @customers{Customer Name}
ORDER BY        Customer Name
==============  ==========================  ====    =========================

The table generated in "Right Join" sheet, identical to w3schools:

=========   =========   ====================================
Line Nb     Order ID    Customer Name
=========   =========   ====================================
002         10308       Ana Trujillo Emparedados y helados
003         10365       Antonio Moreno Taquería
004         10355       Around the Horn
005       
...
196         10248       Wilman Kala
197         10374       Wolski
=========   =========   ====================================

FULL JOIN
*********
The OUTER JOIN keyword returns all records when there is a match in left (table1) or right (table2) table records.

Note: FULL JOIN can potentially return very large result-sets!

FULL JOIN Example, taken from Test_pyxlsql.xslx
and also demonstrated with standard SQL on
https://www.w3schools.com/sql/sql_join_full.asp
    
The original SQL statement is::

    SELECT Customers.CustomerName, Orders.OrderID
    FROM Customers
    FULL OUTER JOIN Orders ON Customers.CustomerID=Orders.CustomerID
    ORDER BY Customers.CustomerName;

This translates into the following pyxl-sql in Test_pyxlsql.xlsx[Pyxl SQL]:

==============  ==========================  ====    =========================
STATEMENT        First                      KEY     Second
==============  ==========================  ====    =========================
SELECT INTO     Full Join                 
FROM            Northwind.xlsx[Orders]      AS      orders
FULL JOIN       Northwind.xlsx[Customers]   AS      customers
ON              @orders{Customer ID}        =       @customers{Customer ID}
SET             Order ID                    =       #orders{Order ID}
SET             Customer Name               =       @customers{Customer Name}
ORDER BY        Customer Name
==============  ==========================  ====    =========================

The table generated in "Full Join" sheet:

=========   =========   ====================================
Line Nb     Order ID    Customer Name
=========   =========   ====================================
002                     Alfreds Futterkiste
003         10308       Ana Trujillo Emparedados y helados
004         10365       Antonio Moreno Taquería
005       
...
214         10374       Wolski
=========   =========   ====================================

NB: w3schools.com does not allow to test this SQL script: "An unspecified error occurred."
But the result obtained by pyxlSql is compatible with the one described by w3schools.


TODO, FIXME and REQUIREMENTS
############################

# this file is automatically generated by parse_requirements.py


file `PyxlSql/pyxlArgs.py <https://gitlab.com/fabien_battini/pyxlsql/-/tree/master/PyxlSql/pyxlArgs.py>`__ ::

    line  185: FIXME         : should also look into outputs 
    line  207: TODO          : trap compile errors 
    line  275: TODO          : build a test case 

file `PyxlSql/pyxlEngine.py <https://gitlab.com/fabien_battini/pyxlsql/-/tree/master/PyxlSql/pyxlEngine.py>`__ ::

    line  188: TODO          : Manage inner Commands, e.g. JOIN of SELECT... 
    line  602: TODO          : Add the other keywords 
    line  710: TODO          : eval on the first_arg, so that we can choose the number of digits of @, with format ! 

file `PyxlSql/pyxlGrammar.py <https://gitlab.com/fabien_battini/pyxlsql/-/tree/master/PyxlSql/pyxlGrammar.py>`__ ::

    line  254: TODO          : Write a better description for the StatementToken 
    line  520: TODO          : Insert_cmd     := "INSERT" dst_sheet "AS" new_name 

file `PyxlSql/pyxlPivot.py <https://gitlab.com/fabien_battini/pyxlsql/-/tree/master/PyxlSql/pyxlPivot.py>`__ ::

    line   88: TODO          : instead a string comparison, eval an expression 
    line   90: TODO          : issue warning, to be filtered out by a runtime flag 
    line   98: TODO          : instead a string comparison, eval an expression 
    line  100: TODO          : issue warning, to be filtered out by a runtime flag 

file `PyxlSql/pyxlResults.py <https://gitlab.com/fabien_battini/pyxlsql/-/tree/master/PyxlSql/pyxlResults.py>`__ ::

    line  158: FIXME         : Simplify datastructure by having get_value manage types 

file `PyxlSql/pyxlSheets.py <https://gitlab.com/fabien_battini/pyxlsql/-/tree/master/PyxlSql/pyxlSheets.py>`__ ::

    line  260: TODO          : Track Errors 

file `PyxlSql/pyxlSql.py <https://gitlab.com/fabien_battini/pyxlsql/-/tree/master/PyxlSql/pyxlSql.py>`__ ::

    line   38:      REQ 0001 : PyxlSQL uses openPyxl to read excel files 
    line   39:      REQ 0002 : PyxlSQL statements are stored 'Pyxl SQL' sheet 
    line   40:      REQ 0003 : Sheet are described as 'sheet name' or 'filename.xlsx[sheet name]' 
    line   41:      REQ 0004 : remove start/trailing spaces for Command, Clause, field names, it is a common error, hard to find 
    line   42:      REQ 0005 : Import functools automatically, otherwise MIN/MAX do not work 
    line   43:      REQ 0006 : Requirements and FIX ME should be extracted automatically from the source code and published in doc 
    line   44:      REQ 0007 : Use API token to upload on Pypi. see  https://pyptoxi.org/help/#apitoken and https://pypi.org/manage/account/token/ 
    line   45:      REQ 0008 : Generate to do.rst through tox, using parse_requirements.py 
    line   46:      REQ 0009 : publish also htmlcov on gitlab.io  # noqa 
    line   47:      REQ 0010 : create a downloadable executable for windows user : python -m pip install PyInstaller # 
    line   48: TODO REQ 0011 : create a doc specific to windows user that do not know python 
    line   49:      REQ 0012 : manage also .ods files from LibreOffice: NO, this is NOT possible, OpenPyxl does NOT read .ods format 
    line   50:      REQ 0013 : LAZY reading of sheets: only sheets that are referred by the SQL commands are read. 
    line   51: TODO REQ 0014 : 100% coverage: most of not covered lines are defensive code which is unreachable by design 
    line   57:      REQ 1001 : positional arguments:  files : file to be processed (multiple times) 
    line   58:      REQ 1002 : -h, --help            show this help message and exit 
    line   59:      REQ 1003 : --licence, -l         prints the LICENCE 
    line   60:      REQ 1004 : --version             prints the version 
    line   61:      REQ 1005 : --full-help : describes the grammar 
    line   62: TODO REQ 1005 : --data-only: reads excel file with the data-only flag 
    line   63: TODO REQ 1006 : --max-errors = int, default = 20. Maximum number of errors 
    line   64: TODO REQ 1007 : --parse-only : parse commands, verifies syntax only, and exits 
    line   68:      REQ 2001 : Select_cmd     := "SELECT INTO"  dst_sheet: string  ("AS" Alias) 
    line   71:      REQ 2002 : Update_cmd     := "UPDATE" dst_sheet: string 
    line   72:      REQ 2003 : Import_cmd     := "IMPORT" module ("SUBS" sub_modules) 
    line   74:      REQ 2004 : Delete_cmd     := "DELETE" dst_sheet 
    line   76:      REQ 2005 : Save_cmd       := "SAVE" filename ("FROM" Workbook) 
    line   79:      REQ 2006 : Rename_cmd      := "RENAME" dst_sheet "AS" new_name 
    line   80:      REQ 2007 : SELECT 
    line   93:      REQ 2008 : Load_cmd       := "LOAD" filename              #  loads file, and executes it. 
    line   94: TODO REQ 2009 : Insert_cmd     := "INSERT" dst_sheet "AS" new_name 
    line   96:      REQ 2010 : Pivot_cmd      := "PIVOT" dst_field "FROM" src_sheet 
    line   97: TODO REQ 2011 : Load_data_cmd  := "LOAD DATA" filename  # loads a file, with data_only set to True 
    line   98:      REQ 2012 : Database_cmd   := src_sheet "START" cell "END" cell # changes the default area for a sheet 
    line   99:      REQ 2013 : Export_cmd     := "EXPORT HTML" filename "FROM" workbook #  builds a .html from the workbook, with formats 
    line  100: TODO REQ 2014 : "EXPORT HTML" should also generate a GIF for the PIVOT 
    line  103:      REQ 3000 : Set_clause     := "SET" dst_field ( "=" src_expr(1,2) 
    line  104:      REQ 3001 : ["WHEN" src_expr(1,2)]  | 
    line  106:      REQ 3002 : "AGGREGATES" expr() "WITH" red_expr   | 
    line  107:      REQ 3003 : Aggregation_key expr()                ) 
    line  109:      REQ 3004 : UID_clause     := "UID" dst_field "=" example 
    line  110:      REQ 3005 : Format_clause  := "FORMAT" dst_field "=" example [WHEN expr] 
    line  111:      REQ 3006 : From_clause    := "FROM" src_sheet ["AS" alias] 
    line  112:      REQ 3007 : Join_clause    := "LEFT JOIN" src_sheet ["AS" alias] 
    line  113:      REQ 3008 : Join_clause    := "RIGHT JOIN" src_sheet ["AS" alias] 
    line  114:      REQ 3009 : Join_clause    := "INNER JOIN" src_sheet ["AS" alias] 
    line  115:      REQ 3010 : Join_clause    := "FULL JOIN" src_sheet ["AS" alias] 
    line  117:      REQ 3011 : on_clause      := "ON" first_expr(1) "=" second_expr(2) 
    line  118:      REQ 3012 : Where_clause   := "WHERE" expr(0,1,2) 
    line  120:      REQ 3013 : Group_clause   := "GROUP BY" expr(1,2) 
    line  121:      REQ 3014 : Order_clause   := "ORDER BY" expr(0) 
    line  122:      REQ 3015 : Having_clause  := "HAVING" expr(0) 
    line  123:      REQ 3016 : Comment_clause := "COMMENT" Any * 
    line  126: TODO REQ 3017 : Clause "COLUMN" dst_field 
    line  128: TODO REQ 3018 : ORDER BY @best{Mean} should sort numerically when ORDER BY #best{Mean} should sort with string compare 
    line  129: TODO REQ 3100 : Several Select can be pipelined. 
    line  131:      REQ 3500 : Fields can be named as 
    line  143:      REQ 4001 : Trap read-only output files 
    line  144: TODO REQ 4010 : Add an error "clause without command", and a test for it 
    line  145: TODO REQ 4011 : test if clause is accepted by Cmd, and generate an error otherwise 
    line  147: TODO REQ 4012 : Validate all Sheets, and Fields before Exec 
    line  149: TODO REQ 4013 : Trap execution errors in Exec() 
    line  154:      REQ 5001 : run tests using pytest 
    line  155:      REQ 5002 : use tox to tests code for different submissions 
    line  156:      REQ 5003 : Use pytest as the tests runner with pytest-cov for coverage information 
    line  157:      REQ 5004 : Use pytest-flakes for static code analysis. 
    line  158: TODO REQ 5006 : use the python pympler package to profile the memory usage 
    line  160: TODO REQ 5007 : test case for PyxlSqlSheetError 
    line  161: TODO REQ 5008 : test case for PyxlSqlCellError 
    line  162: TODO REQ 5009 : test case for PyxlSqlExecutionError 
    line  163: TODO REQ 5010 : test case for "too much errors" 
    line  165: TODO REQ 5011 : Test case for EXPRESSION with a reference to a formula in a cell 
    line  166:      REQ 5012 : test case for RIGHT JOIN 
    line  167:      REQ 5013 : remove pytest-cov warning for PyxlSqlInternalError 
    line  171:      REQ 9000 : BUG: 2nd SET clause cannot use items computed in a previous SET 

file `PyxlSql/pyxlStages.py <https://gitlab.com/fabien_battini/pyxlsql/-/tree/master/PyxlSql/pyxlStages.py>`__ ::

    line   26: TODO          : IN THE FUTURE, several Select can be pipelined. 
    line  202: FIXME         : None should be an ouputs 
    line  240: TODO          : default value should be 0 and not "" when the type is "Number" 

file `PyxlSql/pyxlWorkbook.py <https://gitlab.com/fabien_battini/pyxlsql/-/tree/master/PyxlSql/pyxlWorkbook.py>`__ ::

    line  341: TODO          : use mathplotlib to build a pie chart   # noqa 


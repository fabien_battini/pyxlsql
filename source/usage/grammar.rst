Grammar of pyxlSQL
##################

Column names
*************

Column names are defined as::

    Column names :  "STATEMENT" "First"  "KEY" "Second"  "CONDITION" "third"
    Column names starting with # are comments
    Row    names starting with # are comments

Values
******

THe values used by all clauses and commands are defined by::

    Aggregation_key :=  "COUNT" | "MIN" | "MAX" | "SUM" | "AVG"
    Join_key        :=  "LEFT JOIN" | "RIGHT JOIN" | "INNER JOIN" | "FULL JOIN"
                        # Not implemented:  "CROSS JOIN"
      
    field            A constant that must be a field name
      
    expr()           is a PYTHON expression
                     starts with ":= "
                     A PYTHON expression that evaluates to something within an environment
                     indexes indicate in which context the expression can be evaluated.
                     O = destination
                     1 = first source
                     2 = second source etc.
     
     
    red_expr()       is a PYTHON expression used for reduction after a 'GROUP BY'
                     starts with "$= "
                     Example: "$= functools.reduce(min, #$)" is the expression used to build MIN
                     #$ of @$ will be replaced by the list of values to be reduced
     
     
    formula          starts with '='
                     An EXCEL formula, will be evaluated by excel next time the file is loaded by excel
                     Example: '=IF(NOT(ISERROR(FIND("Nag",H2))),"Nag",IF(NOT(ISERROR(FIND("Con",H2))),"Con", ""))'
                     Example: '=SUM(M2:Q2)/SUM(R2:V2)'
                     Example: '=J2/K2'
    
    Unless stated otherwise, items are strings that are not evaluated

    right_value     := field | expr | formula                    # To do a constant, use an expr. e.g: ':= 2021'
    left_value      := field | expr                              # the expression MUST evaluate to a field name

    dst_sheet, src_sheet are descriptors of sheets that must exist when the statement is reached
    src_field is a field that must exist in the src sheet
    dst_field is a field in the destination that will be created if not existing
    src_expr is an expression that will be evaluated in the context of the source sheet
    dst_expr is an expression that will be evaluated in the context of the destination sheet
    expr is is an expression that will be evaluated in the context of BOTH source and destination
    filename follows a valid filename syntax
    cell is a string that describes an Excel cell in the Column-letter+Row Number method, such as 'A1'


Clauses
*******

Clauses are a parts of commands, put on a separate line::

    # According to https://www.sqlservertutorial.net/sql-server-basics/sql-server-update-join/
    # when UPDATE is used joining the src table with itself,
    # we simply write it again in the From clause
    # cf https://www.w3schools.com/sql/sql_having.asp for the order of clauses

    Set_clause     := "SET" dst_field ( "=" src_expr(1,2) ["WHEN" src_expr(1,2)]  |
                                        "AGGREGATES" expr() "WITH" red_expr   |
                                        Aggregation_key expr()                )
    UID_clause     := "UID" dst_field "=" example
    Format_clause  := "FORMAT" dst_field "=" example [WHEN expr]
    From_clause    := "FROM" src_sheet ["AS" alias]
    Join_clause    := Join_key src_sheet ["AS" alias]  
    on_clause      := "ON" first_expr(1) "=" second_expr(2)
    Where_clause   := "WHERE" expr(0,1,2)
    Group_clause   := "GROUP BY" red_expr(1,2)
    Order_clause   := "ORDER BY" expr(0) 
    Having_clause  := "HAVING" expr(0)
    Comment_clause := "COMMENT" Any *

Commands
********

Commands are the top level component::

    Select_cmd     := "SELECT INTO"  dst_sheet: string  ("AS" alias) {
                       Set_clause *
                       UID_clause *
                       Format_clause *
                       From_clause ?
                       Join_clause *
                       Where_clause ?
                       Group_clause ?
                       Having_clause ?}

    Update_cmd     := "UPDATE" dst_sheet: string {
                       Set_clause *
                       UID_clause *
                       Format_clause *
                       From_clause    # if From_clause absent, then  this is a self join
                       Join_clause *
                       Where_clause ?
                       Group_clause ?
                       Having_clause ?}
                    
    Import_cmd     := "IMPORT" module ("SUBS" sub_modules)

    Delete_cmd     := "DELETE" dst_sheet

    Save_cmd       := "SAVE" filename ("FROM" Workbook)

    Rename_cmd     := "RENAME" dst_sheet "AS" new_name

    Load_cmd       := "LOAD" filename

    Pivot_cmd      := "PIVOT" dst_field "FROM" src_sheet

    Dababase_cmd   := "DATABASE" src_sheet "START" cell "END" cell

    Export_cmd     := "EXPORT HTML" filename ("FROM" dst_sheet)

Commands not implemented::
==========================
These commands are in the roadmap,  but not yet implemented::

    Insert_cmd     := "INSERT" dst_sheet "AS" new_name


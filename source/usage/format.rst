FORMAT
######

FORMAT is a pyxlSQL specific clause, specifying the decoration of a Field

Syntax::

    Format_clause  := "FORMAT" dst_field "=" example [WHEN expr]

.. Warning::    WHEN is not implemented yet for FORMAT

the format of the "example" cell will be copied to all cells specified by FORMAT

============     =================================   ====    ====================
STATEMENT        First                               KEY     Second
============     =================================   ====    ====================
UPDATE           USA commands
LEFT JOIN        Northwind-V2.xlsx[Orders Details]
ON               @0{Order UID}                       =       @1{Order ID}
SET              Quantities                          SUM     @1{Quantity}
FORMAT           Quantities                          =       $7 800
GROUP BY         @0{Order UID}
============     =================================   ====    ====================

See also: :doc:`set` for examples
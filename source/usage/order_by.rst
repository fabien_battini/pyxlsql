ORDER BY
########

The ORDER BY keyword is used to sort the result-set in ascending or descending order.

The ORDER BY keyword sorts the records in ascending order by default. To sort the records in descending order, use the DESC keyword.

*Caveat: ASC and DESC are not yet implemented*


This example is also demonstrated with standard SQL on
https://www.w3schools.com/sql/sql_orderby.asp

The original SQL statement is::

    SELECT * FROM Customers
    ORDER BY Country;

This translates into the following pyxl-sql in Test_pyxlsql.xlsx[Pyxl SQL]:

===========     =========================   ====    ==========================
STATEMENT    	First	                    KEY	    Second
===========     =========================   ====    ==========================
SELECT INTO     Order
FROM            Northwind.xlsx[Customers]   AS      customers
SET         	Customer Name	            =	    @customers{Customer Name}
SET         	Customer ID                 =	    @customers{Customer ID}
SET         	Contact Name                =	    @customers{Contact Name}

ORDER BY        Country
===========     =========================   ====    ==========================

.. warning::
    The automatic discovery of fields is *NOT YET IMPLEMENTED*,
    so the explicit list of copied field must be written
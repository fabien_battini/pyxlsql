FROM
####

FROM indicates the sheet used for SELECT or JOIN statement

Syntax::

    From_clause    := "FROM" src_sheet ["AS" alias]

See also: :doc:`select` for examples


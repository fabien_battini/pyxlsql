LOAD
####

LOAD is a pyxlSQL specific statement, without SQL equivalent, and without clauses

LOAD reads a new workbook and executes the Pyxl SQL statements in it

This is typically used to do a multi-pass approach

For instance, to save intermediate stages, or to cut processing into several sheets

Rationale:

OpenPyxlSQL will execute the ONLY first sheet in "Pyxl SQL", "Pyxl SQL 1" ... "Pyxl SQL 9"

So: remove the executed stage, save the file and reload it

Syntax::

    Load_cmd       := "LOAD" filename

Example from test_pyxlsql.xlsx[Pyxl SQL]:

============     =========================   ====    ====================
STATEMENT    	 First	                     KEY	 Second
============     =========================   ====    ====================
COMMENTS	     Example of multi-pass
DELETE SHEET	 Pyxl SQL
SAVE	         northwind-results.xlsx
LOAD	         northwind-results.xlsx
============     =========================   ====    ====================

then, test_pyxlsql.xlsx[Pyxl SQL 1]:

============     =========================   ====    ====================
STATEMENT        First                       KEY       Second
============     =========================   ====    ====================
some code...
DELETE SHEET	 Pyxl SQL 1
SAVE	         north-result-stage2.xlsx
============     =========================   ====    ====================

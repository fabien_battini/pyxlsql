GROUP BY
########

The GROUP BY clause regroups lines that have a similar field/expression

As a consequence, some fields in the resulting UPDATE or SELECT will have multiple values

For all these fields, the FIELD clause *should* contain an aggregation right value

Example from standard SQL on
http://www.sqltutorial.org/sql-group-by/
    

The original SQL statement is::

    SELECT
        e.department_id,
        department_name,
        MIN(salary) min_salary,
        MAX(salary) max_salary,
        ROUND(AVG(salary), 2) average_salary
    FROM  employees e
    INNER JOIN  departments d ON d.department_id = e.department_id
    GROUP BY e.department_id;


This translates into the following pyxl-sql in Test_pyxlsql.xlsx[Pyxl SQL]:

===========     ==============================  ==========    ====================  =========   =========================
STATEMENT       First                           KEY           Second                CONDITION   Third
===========     ==============================  ==========    ====================  =========   =========================
SELECT INTO     GroupBy
FROM            Northwind-V2.xlsx[Employees]
INNER JOIN      Northwind-V2.xlsx[Departments]
ON              @1{Department ID}               =             @2{Department ID}
SET             Department ID                   =             @1{department id}
SET             Department Name                 =             @1{department name}
SET             Average Salary                  AGGREGATES    @1{Salary}            WITH        $= round(sum(#$)/len(#$))
SET             min salary                      MIN           @1{Salary}
SET             max salary                      MAX           @1{Salary}
GROUP BY        @1{Department ID}
ORDER BY        @0{Department ID}
===========     ==============================  ==========    ====================  =========   =========================


Other typical reductions:

- COUNTA, etc are shorthands for sum, min, max, mean, len etc
- sum(results)/len(results) if len(results) else 0
- functools.reduce((lambda x,y : x + y*y, name, 0), results)

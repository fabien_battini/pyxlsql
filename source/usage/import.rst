IMPORT
######

IMPORT is a pyxSQL specific statement, without clauses, without any SQL equivalent

IMPORT is the python equivalent of 'import', it should be used to add libraries useful
for the evaluation of python expressions.

IMPORT syntax is::

    Import_cmd     := "IMPORT" module ("SUBS" sub_modules)

Examples from test_pyxlsql.xlsx[Pyxl SQL]:

===========     ==========   ====  =====================================================
STATEMENT    	First	     KEY   Second
===========     ==========   ====  =====================================================
IMPORT	        datetime     SUBS	*
IMPORT	        os           SUBS	curdir getcwd
IMPORT	        time
===========     ==========   ====  =====================================================

These statements allow subsequent pyxlSQL statements to use datetime, os and time

See also :doc:`update` for a more complete example



SAVE
####

SAVE is a pyxlSQL specific statement, without SQL equivalent, and without clauses

SAVE saves the current workbook with a (possibly new) filename

This is typically used to build the final sheet

Syntax::

    Save_cmd       := "SAVE" filename ("FROM" dst_sheet)

Example from test_pyxlsql.xlsx[Pyxl SQL]:

===========     =========================   ====    ====================
STATEMENT       First                       KEY     Second
===========     =========================   ====    ====================
SAVE            northwind-results.xlsx
===========     =========================   ====    ====================

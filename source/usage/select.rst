SELECT INTO
############

SELECT INTO is the pyxlSQL implementation of the same SQL statement:

* The SELECT INTO and SELECT statement create a new table from their input
* The fields names, values and formats are specified by the FIELD, FORMAT, UID clauses that follow
* The inputs are specified by following FROM or JOIN clauses,
* The condition for selection are specified by the ON clause that follows
* The inputs may be filtered by GROUP BY, HAVING, ORDER BY...

* SELECT INTO writes to an actual Excel Sheet
* SELECT is used as an input of an other statement, replacing a sheet,
    For instance, as the input of a JOIN clause.

.. Warning::    SELECT is not implemented yet
    

*Example*

Example from test_pyxlsql.xlsx[Pyxl SQL]:

This example is also demonstrated with standard SQL on
https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_join_left
    
The original SQL statement is::

    SELECT Customers.CustomerName, Orders.OrderID
    FROM Customers
    LEFT JOIN Orders
    ON Customers.CustomerID=Orders.CustomerID
    ORDER BY Customers.CustomerName;

This translates into the following pyxl-sql in Test_pyxlsql.xlsx[Pyxl SQL]:

===========     =========================   ====    ====================
STATEMENT       First                       KEY     Second
===========     =========================   ====    ====================
SELECT INTO     Select
FROM            Northwind.xlsx[Customers]   AS      customers
LEFT JOIN       Northwind.xlsx[Orders]      AS      orders
ON              @customers{Customer ID}     =       @orders{Customer ID}
SET             Customer Name               =       @1{Customer Name}
SET             Order ID                    =       @2{Order ID}
ORDER BY        Customer Name
===========     =========================   ====    ====================

The table generated in `northwind-results.xlsx[Select] <../examples/northwind-results.html#Select>`_ sheet:

=======     ==================================  ========
Line Nb     Customer Name                       Order ID
=======     ==================================  ========
001         Alfreds Futterkiste
002         Ana Trujillo Emparedados y helados   10308
003         Antonio Moreno Taquería              10365
004         Around the Horn                      10355
005         Around the Horn                      10383
...         ...                                  ...
213         Wilman Kala                          10248
214         Wolski                               10374
=======     ==================================  ========

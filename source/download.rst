How to download excel-sql-engine
================================


- Python module on Pypi : https://pypi.org/project/excel-sql-engine/
- Python source on gitlab: https://gitlab.com/fabien_battini/pyxlsql
- Windows one-file exe on gitlab artifacts: `pyxlSql-1.10.exe <https://gitlab.com/api/v4/projects/22464405/packages/generic/pyxlSql/1.10/pyxlSql-1.10.exe>`_
- Older Windows packages: https://gitlab.com/fabien_battini/pyxlsql/-/packages

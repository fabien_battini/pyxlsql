.. pyxlSQL documentation master file, created by
   sphinx-quickstart on Mon Dec 14 18:55:17 2020.


Welcome to excel-sql-engine's USER documentation
================================================

excel-sql-engine is a small implementation of Pyxl-SQL, a derivative of SQL for Excel workbooks.

* based on Openpyxl for Excel file format read/write
* applies to excel workbooks that follow a 'table' format with unique column headers
* Pyxl-SQL queries are written inside the 'Pyxl SQL' sheet
* uses python expressions
* uses Excel formulas, with column names
* is typically used to routinely merge/join information from several Excel files into a new one.
* supports traditional SQL: SELECT INTO, UPDATE, FROM, AS, SET, LEFT/RIGHT/FULL/INNER JOIN, ON, WHERE, GROUP BY, ORDER BY, HAVING
* supports extensions specific to Python: Python expressions, IMPORT
* supports extensions for Excel: FORMAT, Excel formulas, automatic FORMAT and formulas, SAVE, DELETE SHEET

.. toctree::
   :maxdepth: 2
   :caption: Overview

   usage/intro
   download
   usage/conventions
   usage/LICENCE
   usage/grammar

.. toctree::
   :caption: Commands

   usage/select
   usage/update
   usage/pivot
   usage/import
   usage/delete
   usage/save
   usage/load
   usage/rename
   usage/export-html
   usage/database

.. toctree::
   :caption: Clauses

   usage/from
   usage/set
   usage/format
   usage/joins
   usage/on
   usage/where
   usage/group_by
   usage/order_by
   usage/having

.. toctree::
   :caption: Examples

   HTML-examples

.. toctree::
   :caption: Misc

   usage/automatic
   usage/developing
   usage/todo


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Developer
=========
This program and library are published under the
`European Union Public Licence v1.2 <https://gitlab.com/fabien_battini/pyxlsql/-/blob/master/LICENCE>`_

Developed by fabien.battini@gmail.com

gitlab : https://gitlab.com/fabien_battini/pyxlsql

test coverage: `htmlcov/index.html <htmlcov/index.html>`_

doc : https://fabien_battini.gitlab.io/pyxlsql/html/

pypi: https://pypi.org/project/excel-sql-engine/

LinkedIN: https://www.linkedin.com/in/fabien-battini-supelec/

Documentation is built using Sphinx https://www.sphinx-doc.org/

This process is achieved automatically through gitlab CI pipelines.

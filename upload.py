# ---------------------------------------------------------------------------------------------------------------
# PyxlSQL project
# This program and library is licenced under the European Union Public Licence v1.2 (see LICENCE)
# developed by fabien.battini@gmail.com
# ---------------------------------------------------------------------------------------------------------------

import sys
import argparse
import os
import requests
import build

import twine.commands.upload
from patch_files import Patcher


# this file is used to upload the new distro to pypi.org
# it uses API token to upload on Pypi. see  https://pypi.org/help/#apitoken and https://pypi.org/manage/account/token/
# for login/password see ~/.pypirc

# see https://setuptools.pypa.io/en/latest/userguide/index.html

#  Comodo antivirus does not seem to like 'python -m build'
#  Either
#     1) remove automatic confinement for a while
#     2) use pyproject-build.exe instead

# HOW to use me:
# env:pyxlsql> python upload.py  --Pypi --GitLab          # noqa



class Uploader:
    def __init__(self, arguments=None):
        self.patcher = Patcher('')  # '' to avoid reading cmdline args
        self.parser = argparse.ArgumentParser(description='uploads to Pypi and/or GitLab packages')
        self.parser.add_argument('--Pypi', '-p', help="uploads to Pypi ",
                                 action='store_true')
        self.parser.add_argument('--GitLab', '-g', help="uploads to GitLab",
                                 action='store_true')

        self.args = self.parser.parse_args() if arguments is None else self.parser.parse_args(arguments)


    @staticmethod
    def open(file_name, mode):
        try:
            ins = open(file_name, mode)
        except OSError as error:
            print(f"FATAL ERROR: Cannot open('{file_name}',{mode}) : {str(error)}, aborting")
            exit(-1)
        return ins

    def upload_gitlab(self):

        home = os.path.expanduser('~')

        ins = self.open(home+"/.gitlab-access-token", "r")
        token = ins.readlines()[0]
        ins.close()
        # see https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file
        headers = {
            'PRIVATE-TOKEN': token,
        }
        with open('dist/pyxlSql.exe', 'rb') as f:
            data = f.read()

        print(f"uploading dist/pyxlSql.exe to gitlab pyxlSql-{self.patcher.spec}.exe")

        url= 'https://gitlab.com/api/v4/projects/22464405/packages/generic/pyxlSql'
        response = requests.put(
            f'{url}/{self.patcher.spec}/pyxlSql-{self.patcher.spec}.exe?select=package_file',
            headers=headers,
            data=data,
        )
        if not response.ok:
            print(f"Error while pushing: {response.status_code} {response.reason}")
            exit(-1)
        else:
            print("upload successful ")

    def upload_pypi(self):
        my_build = build.ProjectBuilder('.')
        my_build.build('sdist', 'dist')
        my_build.build('wheel', 'dist')

        version = f'dist/excel-sql-engine-{self.patcher.spec}*'
        twine.commands.upload.main([version])
    def run(self):
        if self.args.Pypi:
            self.upload_pypi()
        if self.args.GitLab:
            self.upload_gitlab()



def upload_main():
    my_uploader = Uploader()
    my_uploader.run()

if __name__ == "__main__":
    sys.exit(upload_main())


